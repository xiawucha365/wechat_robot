<?php
namespace app\components;

use Yii;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;


/**
 * 权限过滤器
 * Class XwcAccessControl
 * @package app\components
 */
class XwcAccessControl extends AccessControl
{

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {

        if (parent::beforeAction($action)) {
            $permission = $action->controller->id . '/' . $action->id;
            if ($this->_allowActions($permission) || Yii::$app->user->can($permission)) {
                return true;
            } else {
                throw new ForbiddenHttpException("抱歉,您没有权限操作这个页面,请联系管理员开通!");
            }
        }
        return false;
    }


    /**
     * 排除的Action 比如ajax请求,错误页面等
     */
    private function _allowActions($permission)
    {

        $list =  [
            'site/error',
            'rabc/getres',
            'site/logout',
            'site/login',
            'site/captcha',
            'file/upload',
            'project/itemvalidate',
            'project/getproductlist',
            'project/getcvelist',
            'project/updateprojectstatus',
            'ajax/*'
        ];

        $bool = false;
        foreach($list as $key => $item){
            if(strstr($item,"*"))
            {
                $arr1 = explode('/',$item);
                $key1 = $arr1[0];
                $arr2 = explode('/',$permission);
               $key3 = $arr2[0];
                if($key1 == $key3) {
                    $bool =  true;
                }
            }elseif($item == $permission){
                $bool =  true;

            }
        }


        return $bool;

    }
}
