<?php

namespace app\components\factory;



use app\components\lib\CWechat;
use app\components\lib\Message;

class MessageFactory {


    public $wechat;
    public $message;
    public function __construct(CWechat $wechat)
    {
        $this->wechat =$wechat;
        if(empty($this->message)){
             $this->message = new Message($wechat);
        }

    }

    public function __destruct()
    {

        // TODO: Implement __destruct() method.
    }


}

