<?php

namespace app\components\lib;
use app\components\CTools;
use GuzzleHttp\Cookie\FileCookieJar;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use GuzzleHttp\Client as GuzzleHttpClient;
/**
 +------------------------------------------------------------------------------
 * Http 工具类
 * 提供一系列的Http方法
 +------------------------------------------------------------------------------
 * @category   ORG
 * @package  ORG
 * @subpackage  Net
 * @author    liu21st <liu21st@gmail.com>
 * @version   $Id: Http.class.php 2504 2011-12-28 07:35:29Z liu21st $
 +------------------------------------------------------------------------------
 */
class CHttpSwoole {

    public static function get($url){
        \Swoole\Async::dnsLookup("api.github.com", function ($host, $ip)use($url) {

            if (stripos($url, "https://") !== FALSE)
            {
                $cli = new \Swoole\Http\Client($ip, 443, true);
            }else{
                $cli = new \Swoole\Http\Client($ip, 80);
            }
            $cli->setHeaders([
                'Host' => $host,
                "User-Agent" => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36',
                'Accept' => '*',
                'Accept-Encoding' => 'gzip,deflate,br',
            ]);
            $cli->set(['timeout' => 10.0]); //设置10秒超时
//            $cli->setMethod("GET");
            $cli->get('/', function ($cli )  {

            });
    });
            }

}

