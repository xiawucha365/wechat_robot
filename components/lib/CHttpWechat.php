<?php

namespace app\components\lib;
use app\components\CTools;
use GuzzleHttp\Cookie\FileCookieJar;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use GuzzleHttp\Client as GuzzleHttpClient;
/**
 +------------------------------------------------------------------------------
 * Http 工具类
 * 提供一系列的Http方法
 +------------------------------------------------------------------------------
 * @category   ORG
 * @package  ORG
 * @subpackage  Net
 * @author    liu21st <liu21st@gmail.com>
 * @version   $Id: Http.class.php 2504 2011-12-28 07:35:29Z liu21st $
 +------------------------------------------------------------------------------
 */
class CHttpWechat {

    public static function curlPostJson($url, $webkit,$param=[], $jsonfmt = true)
    {


        $ch = curl_init($url);
        if (stripos($url, "https://") !== FALSE) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
        }


        $header = [
            'User-Agent: ' . $webkit['user_agent']
        ];
        if ($jsonfmt) {
            $param = CTools::json_encode($param);
            $header[] = 'Content-Type: application/json; charset=UTF-8';
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $webkit['cookie']);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $webkit['cookie']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
//        curl_setopt($ch,CURLOPT_PROXY,'127.0.0.1:8888');

        $result = curl_exec($ch);
        $aStatus = curl_getinfo($ch);

        curl_close($ch);
        if (intval($aStatus["http_code"]) == 200) {
            if ($jsonfmt)
                return json_decode($result, true);
            return $result;
        } else {
            return false;
        }
    }

    public static function curlPost($url, $param, $jsonfmt = true, $webkit = [])
    {

        $ch = curl_init($url);
        if (stripos($url, "https://") !== FALSE) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
        }
        $header = [
            'User-Agent: ' . $webkit['user_agent']
        ];


        if (is_string($param)) {
            $strPOST = $param;
        }else {
            $aPOST = array();
            foreach ($param as $key => $val) {
                $aPOST[] = $key . "=" . urlencode($val);
            }
            $strPOST = implode("&", $aPOST);
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch,CURLOPT_PROXY,'127.0.0.1:8888');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $strPOST);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $webkit['cookie']);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $webkit['cookie']);


        $result = curl_exec($ch);
        $aStatus = curl_getinfo($ch);
        curl_close($ch);
        if (intval($aStatus["http_code"]) == 200) {
            if ($jsonfmt)
                return json_decode($result, true);
            return $result;
        } else {
            return false;
        }
    }

    public static function curlPostFile($url, $webkit,$param=[],$filename)
    {
        $ch = curl_init($url);
        if (stripos($url, "https://") !== FALSE) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
        }


        $header[] = "multipart/form-data";
        curl_setopt($ch, CURLOPT_USERAGENT, $webkit['user_agent']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $webkit['cookie']);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $webkit['cookie']);
        curl_setopt($ch,CURLOPT_REFERER,'wx.qq.com');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);


        //=============//
        $result = curl_exec($ch);
        $aStatus = curl_getinfo($ch);
        curl_close($ch);
        if (intval($aStatus["http_code"]) == 200) {
                 $result = json_decode($result, true);
            if($result['BaseResponse']['Ret'] != 0){
                self::curlPostFile($url, $webkit,$param,$filename);
            }else{
                return $result;
            }

        } else {
            return false;
        }


    }


    public static function curlGet($url, $webkit ="",$params = [], $api = false)
    {
        $oCurl = curl_init();
        if (stripos($url, "https://") !== FALSE) {
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($oCurl, CURLOPT_SSLVERSION, 1);
        }
        $header=[];
        if(!empty($webkit)) {
            $header = [
                'User-Agent: ' . $webkit['user_agent'],
                'Referer: https://wx.qq.com/'
            ];
        }
        if ($api == 'webwxgetvoice')
            $header[] = 'Range: bytes=0-';
        if ($api == 'webwxgetvideo')
            $header[] = 'Range: bytes=0-';
        curl_setopt($oCurl, CURLOPT_HTTPHEADER, $header);
        if (!empty($params)) {
            if (strpos($url, '?') !== false) {
                $url .= "&" . http_build_query($params);
            } else {
                $url .= "?" . http_build_query($params);
            }
        }
        curl_setopt($oCurl, CURLOPT_URL, $url);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($oCurl, CURLOPT_TIMEOUT, 5);
        if(!empty($webkit)){
            curl_setopt($oCurl, CURLOPT_COOKIEFILE, $webkit['cookie']);
            curl_setopt($oCurl, CURLOPT_COOKIEJAR, $webkit['cookie']);
        }

        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);
        curl_close($oCurl);
        if (intval($aStatus["http_code"]) == 200) {
            return $sContent;
        } else {
            return false;
        }
    }
    public static function br2nl($string)
    {
        return preg_replace('/\<br(\s*)?\/?\>/i', PHP_EOL, $string);
    }

}

