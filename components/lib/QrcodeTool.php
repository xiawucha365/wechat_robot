<?php

namespace app\components\lib;

use PHPQRCode\Constants;
use PHPQRCode\QRcode;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\ConsoleOutput;
class QrcodeTool {

    public static function cmdqrcode($weuuid)
    {
        $url = "https://login.weixin.qq.com/l/".$weuuid;
        $output = new ConsoleOutput();

        $style = new OutputFormatterStyle('black', 'black');
        $output->getFormatter()->setStyle('blackc', $style);
        $style = new OutputFormatterStyle('white', 'white');
        $output->getFormatter()->setStyle('whitec', $style);
        $output->writeln("<whitec>  </whitec><blackc>  </blackc><whitec>  </whitec>");

        $pxMap[0] = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ? '<whitec>mm</whitec>' : '<whitec>  </whitec>';
        $pxMap[1] = '<blackc>  </blackc>';

        $text = QRcode::text($url);

        $length = strlen($text[0]);

        $output->write("\n");
        foreach ($text as $line) {
            $output->write($pxMap[0]);
            for ($i = 0; $i < $length; $i++) {
                $type = substr($line, $i, 1);
                $output->write($pxMap[$type]);
            }
            $output->writeln($pxMap[0]);
        }
    }




}

