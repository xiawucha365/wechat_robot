<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/15
 * Time: 12:57
 */

namespace app\commands;

use app\components\lib\QrcodeTool;
use yii\console\Controller;
use app\components\lib\CWechat;

class RobotController extends Controller
{
    public function actionIndex($session,$type="start")
    {

        $obj = new CWechat($session,$type);
        #初始化
//        $obj->showQRCodeImg();
        $obj->syncSessions();
        $obj->webwxinit();
        $obj->webwxstatusnotify();
        $obj->gettxlMembers();
        $obj->gethhMembers();
        $obj->listenMsgMode();

    }
}