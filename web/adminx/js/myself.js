(function () {
    "use strict";
    /*
     * domInit 渲染的DOM
     * domHtml
     *
     * */
    var ajaxpage = function (domInit, callbackFunc, options) {
        this.ops = options;
        this.domInit = domInit;
        var _this = this;
        this.init = function () {
            this.runNow();
        };
        this.runNow = function () {
            $(_this.domInit).pagination({
                totalData: _this.ops.totalCount,			//数据总条数
                showData: _this.ops.showDataCount,				//每页显示的条数
                coping: true,
                homePage: '首页',
                endPage: '末页',
                callback: function (api) {
                    //一般直接写在一个js文件中
                    var layer = layui.layer;
                    layer.load(1, {
                        shade: [0.2, '#bdbdbd'] //0.1透明度的白色背景
                    });
                    callbackFunc(api, _this);
                }
            }, function (api) {
                callbackFunc(api, _this);
            });
        };


    }


    var myselfEventType = function () {

        //版本列表
        this.renderVersion = function (api, _this) {
            var data = {
                page: api.getCurrent(),
                item_id: $("#item_id").val(),
                pagesize:_this.ops.showDataCount,
                search: {}
            };
            $.getJSON('/index.php?r=ajax/getversions', data, function (json) {
                if (json.length == 0) {
                    $(_this.domInit).parent().parent().parent().prev('table').find("tbody").html("");
                    $(_this.domInit).parent().parent().parent().prev('table').find("tbody").html("<tr><td colspan='3'>暂无数据</td></tr>");
                } else {
                    var html = "";
                    //细节渲染
                    json.forEach(function (e) {
                        html += "<tr><td>" + e.vid + "</td>\
                                <td>" + e.version + "</td>\
                                <td>" + e.standard_version + "</td>\
                                <td><a href='" + e.url + "' target='_blank'>下载地址</a></td>\
                                <td>" + e.status + "</td>\
                            <td><div class='btn-group btn-group-sm'>\
                                <a class='btn btn-default editversion'  type='button' data-vid='" + e.vid + "' href='javascript:void(0);'>编辑</a>\
                                <a class='btn btn-danger delversion' type='button' data-vid='" + e.vid + "'>删除</a>\
                                </div></td></tr>";
                    });

                    $(_this.domInit).parent().parent().parent().prev('table').find("tbody").html(html);
                }
                //关闭加载
                layer.closeAll(); //关闭加载层
            });


        }

        //Cpe列表
        this.renderCpe = function (api, _this) {
            //第一次加载
            var data = {
                page: api.getCurrent(),
                item_id:$("#item_id").val(),
                pagesize:_this.ops.showDataCount,
            };
            $.getJSON('/index.php?r=ajax/getcpes', data, function (json) {
                if(data.length === 0){
                    $(_this.domInit).parent().parent().parent().prev('table').find("tbody").html("");
                    $(_this.domInit).parent().parent().parent().prev('table').find("tbody").html("<tr><td colspan='3'>暂无数据</td></tr>");
                }else{
                    var html = "";
                    json.forEach(function(e){
                        html +="<tr><td>"+ e.id+"</td><td>"+ e.version+"</td><td>"+ e.cpe+"</td></tr>";
                    });
                    $(_this.domInit).parent().parent().parent().prev('table').find("tbody").html(html);
                }

                layer.closeAll(); //关闭加载层
            });
        }


        //Tag列表
        this.renderTag = function (api, _this){
            var data = {
                page: api.getCurrent(),
                item_id:$("#item_id").val(),
                pagesize:_this.ops.showDataCount,
                search:{}
            };

            if(_this.ops.keyword){
                data.search.version = _this.ops.keyword;
            }

            $.getJSON('/index.php?r=ajax/getversiontags', data, function (json) {
                if(json.totalCount == 0){
                    $(_this.domInit).parent().parent().parent().prev('table').find("tbody").html("");
                    $(_this.domInit).parent().parent().parent().prev('table').find("tbody").html("<tr><td colspan='3'>暂无数据</td></tr>");
                }else{
                    var html = "";
                    json.list.forEach(function(e){
                        html +="<tr><td>"+ e.vid+"</td><td>"+ e.version+"</td><td><a class='tagxifu'   data-tagid='"+e.vid+"'  href='javascript:void(0);'>吸附</a></td></tr>";
                    });
                    $(_this.domInit).parent().parent().parent().prev('table').find("tbody").html(html);
                }
                layer.closeAll(); //关闭加载层
            });

        }
    }


    var myselfEventTypeObj = new myselfEventType();
    var layer = layui.layer;
    /*========================================================TAG初始化====================*/
    var options = {totalCount:parseInt($("#tag_count").val()),showDataCount:5};
    var pageAjaxTag = new ajaxpage($("#paginationTag"),myselfEventTypeObj.renderTag,options);
    pageAjaxTag.init();
    /*========================================================CPE初始化====================*/
    var options = {totalCount:parseInt($("#cpe_count").val()),showDataCount:5};
    var pageAjaxCpe = new ajaxpage($("#paginationCpe"),myselfEventTypeObj.renderCpe,options);
    pageAjaxCpe.init();
    /*=====================================================版本初始化==============================================================*/
    var options = {totalCount:parseInt($("#version_count").val()),showDataCount:12};
    var pageAjaxVersion = new ajaxpage($("#paginationVersion"),myselfEventTypeObj.renderVersion,options);
    pageAjaxVersion.init();

    /*=====================================================添加版本==============================================================*/

    $('#itemadd').on('click', function () {
        //iframe层-禁滚动条
        layer.open({
            maxmin: true,
            title: '添加版本',
            shade: 0.6,
            type: 2,
            area: ['50%', '600px'],
            content: ["index.php?r=project/versioncreate&item_id=" + $("#item_id").val(), 'yes'],
            end: function (index) {
                var data = {page: 1, item_id: $("#item_id").val(), search: {}};
                $.getJSON('/index.php?r=ajax/getversionscount', data, function (json) {
                    var totalCount = json.count;
                    var options = {totalCount: totalCount, showDataCount: 12};
                    var pageAjax = new ajaxpage($("#paginationVersion"), myselfEventTypeObj.renderVersion, options);
                    pageAjax.init();
                });
            }
        });
    });


    /*=====================================================编辑版本==============================================================*/
    $(document).on('click', ".editversion", function () {
        var vid = $(this).data("vid");
        //iframe层-禁滚动条
        layer.open({
            maxmin: true,
            title: '编辑版本',
            shade: 0.6,
            type: 2,
            area: ['50%', '600px'],
            content: ["index.php?r=project/versionedit&vid=" + vid, 'yes'],
            end: function (index) {
                var data = {page: 1, item_id: $("#item_id").val(), search: {}};
                $.getJSON('/index.php?r=ajax/getversionscount', data, function (json) {
                    var totalCount = json.count;
                    var options = {totalCount: totalCount, showDataCount: 12};
                    var pageAjax = new ajaxpage($("#paginationVersion"), myselfEventTypeObj.renderVersion, options);
                    pageAjax.init();
                });
                ////do something
                //window.location.reload();
            }
        });
    });


    /*=====================================================删除版本==============================================================*/
    $(document).on('click', ".delversion", function () {
        var vid = $(this).data("vid");
        layer.msg('确定删除吗?', {
            time: 0 //不自动关闭
            , btn: ['确定', '取消']
            , yes: function (index) {
                layer.close(index);
                window.location.href = "index.php?r=project/versiondel&vid=" + vid;
            }
        });


    });

    /*=====================================================吸附点击==============================================================*/
    $(document).on('click', ".tagxifu", function () {
        var tagid = $(this).data('tagid');
        //iframe层-禁滚动条
        layer.open({
            maxmin: true,
            title: '添加版本',
            shade: 0.6,
            type: 2,
            area: ['50%', '600px'],
            content: ["index.php?r=project/versioncreate&item_id=" + $("#item_id").val() + "&tagid=" + tagid, 'yes'],
            end: function (index, layero) {
                var data = {page: 1, item_id: $("#item_id").val(), search: {}};
                $.getJSON('/index.php?r=ajax/getversionscount', data, function (json) {
                    var totalCount = json.count;
                    var options = {totalCount: totalCount, showDataCount: 12};
                    var myselfEventTypeObj = new myselfEventType();
                    var pageAjax = new ajaxpage($("#paginationVersion"), myselfEventTypeObj.renderVersion, options);
                    pageAjax.init();
                });
            }
        });

    });


    //联动触发
    $(document).on('keyup', "#versiontagsearch", function (){
    //$("#versiontagsearch").keyup(function(){
        //暂不做分页
        var data = {page: 1, item_id:$("#item_id").val(), search:{}};
        if($("#versiontagsearch").val()) data.search.version = $("#versiontagsearch").val();

        $.getJSON('/index.php?r=ajax/getversiontagscount', data, function (json) {
            var totalCount  = json.count;
            var options = {totalCount:totalCount,showDataCount:5,keyword:$("#versiontagsearch").val()};
            var pageAjax = new ajaxpage($("#paginationTag"),myselfEventTypeObj.renderTag,options);
            pageAjax.init();
        });

    });





    //function test(api){
    //    console.log(api);
    //}
    //
    //var testObj = function(Func){
    //    this.Func = Func;
    //    var _this = this;
    //    this.init = function(){
    //        _this.Func('ds');
    //
    //    }
    //}
    //var ppp = new  testObj(test);
    //ppp.init();


})();