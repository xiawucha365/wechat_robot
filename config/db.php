<?php


return [
    'class' => 'yii\db\Connection',

    // 配置主服务器
    'dsn' => 'mysql:host=127.0.0.1;dbname=code_house_new',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

    // 配置从服务器
    'slaveConfig' => [
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'attributes' => [
            // use a smaller connection timeout
            PDO::ATTR_TIMEOUT => 10,
        ],
    ],

    // 配置从服务器组
    'slaves' => [
        ['dsn' => 'mysql:host=127.0.0.1;dbname=code_house_new'],
    ]

];